
def readFile():
    file_name = './d_pet_pictures.txt'
    try:
        with open(file_name,'r') as f:
            lines =  f.readlines()
            dict = {}
            for index,item in enumerate(lines):
                if index ==0:
                    continue
                temp = item.split()
                orientation = temp[0]
                image_id = temp[1]
                tags= []
                for i in range(2,len(temp)):
                    tags.append(temp[i])

                dict[index] = {'orientation': orientation, 'tags': tags}
            # print(dict)
            return dict
    except EOFError:
        print("error")


    # list with lists : [[3], [0], [1,2]]
def write_file(solution):
    with open('solution.txt', 'w') as file:
        file.writelines('%i\n' % (len(soluction)))
        for index,item in enumerate(solution):
            for i,inner in enumerate(item):
                # print(i, inner)
                file.writelines('%i' % (inner-1))
                if i == len(item)-1 :
                    file.writelines('\n')
                else:
                    file.writelines(' ')


def comp(dict_images, solution):
    temp_set = set()
    for item in dict_images:
        if item in temp_set:
            continue
        if dict_images[item]['orientation'] == 'V':
            for inner in range(item +1, len(dict_images)-1):
                if dict_images[inner]['orientation'] == 'H' or inner in temp_set:
                    continue
                else:
                    if len(temp_set) == 0:
                        list1 = dict_images[item]['tags']
                        list2 = dict_images[inner]['tags']
                        x = list(set(list1).intersection(list2))
                        if len(x) !=0:
                            temp_set.add(inner)
                            temp_set.add(item)
                            soluction.append([item, inner])
                            break

                    else:
                        last_index = len(soluction) -1
                        if len(soluction[last_index]) == 1:
                            list1 = dict_images[item]['tags']
                            list2 = dict_images[inner]['tags']
                            list3 = dict_images[soluction[last_index][0]]['tags']
                            x = list(set(list1).intersection(list2))
                            if len(x) != 0:
                                y = list(set(x).intersection(list3))
                                if len(y) !=0:
                                    temp_set.add(item)
                                    temp_set.add(inner)
                                    soluction.append([item, inner])
                                    break
                        else:
                            list1 = dict_images[item]['tags']
                            list2 = dict_images[inner]['tags']
                            x = list(set(list1).intersection(list2))
                            if len(x) != 0:
                                item1= dict_images[soluction[last_index][0]]['tags']
                                item2 = dict_images[soluction[last_index][1]]['tags']
                                itemy= list(set(item1).intersection(item2))
                                last_union = list(set(itemy).intersection(x))
                                if len(last_union) !=0:
                                    temp_set.add(item)
                                    temp_set.add(inner)
                                    soluction.append([item, inner])
                                    break

        elif dict_images[item]['orientation'] == 'H':
            if len(temp_set) == 0:
                temp_set.add(item)
                soluction.append([item])
            else:
                list1 = dict_images[item]['tags']
                last_index = len(soluction) -1
                if len(soluction[last_index]) == 1: # prin V
                    list2 = dict_images[soluction[last_index][0]]['tags']
                    x = list(set(list1).intersection(list2))
                    if len(x) != 0:
                        temp_set.add(item)
                        soluction.append([item])
                else:
                    list1 = dict_images[soluction[last_index][0]]['tags'] #ggggf
                    list2 = dict_images[soluction[last_index][1]]['tags']
                    x = list(set(list1).intersection(list2))
                    y = list(set(x).intersection(dict_images[item]['tags']))
                    if len(y) !=0:
                        temp_set.add(item)
                        soluction.append([item])

    write_file(soluction)


if __name__ == '__main__':
    soluction = []

    dict_images = readFile()
    comp(dict_images, soluction)

    # with open('solution.txt', 'r') as f:
    #     lines = f.readlines();
